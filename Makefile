
start:
	@cd docker && docker-compose up -d

stop:
	@cd docker && docker-compose down

restart: stop start

bash:
	@cd docker && docker-compose exec --user=www-data contentcache bash
