<?php

namespace Bosi\ContentCache;

class ContentCache
{
    private static $publicBasePath;
    private static $shouldCacheFunction;

    private function __construct()
    {
    }

    /**
     * @param string $publicBasePath absolute path to the webserver root (normally this is where the index.php live)
     * @param callable|null $shouldCacheFunction a function which will be called additionally to check if a response should be cached
     */
    public static function init(string $publicBasePath, ?callable $shouldCacheFunction = null): void
    {
        self::$publicBasePath = $publicBasePath;
        self::$shouldCacheFunction = $shouldCacheFunction;
    }

    /**
     * @param string $content response body which should be cached
     * @return bool true, if content was successfully added to cache
     * @throws InvalidConfigurationException
     */
    public static function addToCache(string $content): bool
    {
        self::checkConfiguration();

        try {
            if (self::shouldCache() === false) {
                return false;
            }

            $fsPath = self::getPath();
            $fsDir = dirname($fsPath);
            if (!is_dir($fsDir)) {
                mkdir($fsDir, 0777, true);
            }

            file_put_contents($fsPath, $content, LOCK_EX);
        } catch (\Exception $e) {
            if (isset($fsPath) && is_file($fsPath)) {
                unlink($fsPath);
            }

            return false;
        }

        return true;
    }

    /**
     * @param string|null $path relative path to $publicBasePath
     * @throws InvalidConfigurationException
     */
    public static function clearCache(?string $path = null)
    {
        self::checkConfiguration();

        $path = trim($path ?? '', '/');

        return system(sprintf('rm -rf %s/%s', self::getBasePath(), $path));
    }

    private static function shouldCache(): bool
    {
        $scf = self::$shouldCacheFunction;
        if (
            $scf !== null
            && is_callable($scf)
            && $scf() === false
        ) {
            return false;
        }

        return empty($_SERVER['QUERY_STRING']);
    }

    private static function getBasePath(): string
    {
        return rtrim(self::$publicBasePath, '/') . '/content-cache';
    }

    private static function getPath(): string
    {
        $fsPath = self::getBasePath();

        $path = explode('?', $_SERVER['REQUEST_URI'])[0];
        $fsPath .= (($path === '/') ? '/__index' : $path);

        return $fsPath . '.file';
    }

    private static function checkConfiguration(): void
    {
        if (empty(self::$publicBasePath) || !is_dir(self::$publicBasePath)) {
            throw new InvalidConfigurationException(sprintf('public base path is empty or no directory. You have to define it via ContentCache::init()'));
        }
    }
}
