# Content Cache
![pipeline status](https://gitlab.com/bosi/content-cache/badges/master/pipeline.svg)

This package provides a caching mechanism on file system level for super fast delivery of cached data.

## Requirements
* composer (for installation)
* at least php 7.3
* apache2 webserver
* some kind of cron to clear cache from time to time

## Installation
```bash
composer require bosi/content-cache
```

## Usage
1. Initialize the cache somewhere in your application bootstrapping
    ```php
   <?php
   use Bosi\ContentCache\ContentCache;
   ContentCache::init('/absolute/path/to/webserver/root'); 
   ```
2. Add cached responses after you have rendered the full response
    ```php
    <?php
    use Bosi\ContentCache\ContentCache;
   
    // somewhere at the and of your controller (e.g. inside middleware before sending response to user)
    ContentCache::addToCache($response);
    ```
3. Enable your apache webserver to serve cached responses by adding the following lines to your `.htaccess`
    ```apacheconfig
    # prevent direct indexing of cache dir
    <If "%{THE_REQUEST} =~ /content-cache.+\.file/">
       Header set X-Robots-Tag "noindex, nofollow"
    </If>
    
    # handle cache for index page
    RewriteCond %{QUERY_STRING} ^$
    RewriteCond %{REQUEST_URI} ^/?$
    RewriteCond %{DOCUMENT_ROOT}/content-cache/__index.file -f
    RewriteRule .? content-cache/__index.file [L]
    
    # handle cache for non-index page
    RewriteCond %{QUERY_STRING} ^$
    RewriteCond %{DOCUMENT_ROOT}/content-cache%{REQUEST_URI}.file -f
    RewriteRule . content-cache%{REQUEST_URI}.file [L]
   ```
4. Add a cronjon to your system to clear the cache from time to time e.g.
    ```
   */5 * * * * rm -rf /absolute/path/to/webserver/root/content-cache/*
   ```
   If you want to clear the cache with your php app you can use `ContentCache::clearCache()` (first parameter can be used to not flush the complete cache).
   
## Nice to know
* Your application is only called when no fitting cached response can be found.
* This package creates a directory called `content-cache` below you defined webserver root. All cached responses are saved inside this directory and you can even delete single files/directories if you want to clear cache for specific paths.
* Requests with url query parameters will not be cached at all (maybe I'll at this later as a feature but currently ther're some problems with that).
* There are no further checks (execpt query params) against status code or http method to decide if a response should be cached. You have to implement such a check by yourself (e.g. by using the second param of `ContentCache::init(...)`.
* You can have a look at the `public/` directly for a full example usage of this package (you can even start a local environment using docker and `make start` to view the result).