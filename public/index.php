<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Bosi\ContentCache\ContentCache;

class Controller
{
    public static function processRequest(): void
    {
        $response = self::getResponse();

        ContentCache::init(__DIR__);
        ContentCache::addToCache($response);

        printf($response);
    }

    private static function getResponse(): string
    {
        return json_encode([
            'data'      => 'hello world',
            'timestamp' => time()
        ]);
    }
}

Controller::processRequest();