<?php

use Bosi\ContentCache\ContentCache;
use PHPUnit\Framework\TestCase;
use Bosi\ContentCache\InvalidConfigurationException;

class ContentCacheTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        system('rm -rf ' . __DIR__ . '/content-cache');

        parent::setUpBeforeClass();
    }

    public function testPath(): void
    {
        $method = $this->getMethod('getPath');

        $_SERVER = ['REQUEST_URI' => '/'];
        self::assertEquals(
            __DIR__ . '/content-cache/__index.file',
            $method->invoke(new stdClass())
        );

        $_SERVER = ['REQUEST_URI' => '/hello/world'];
        self::assertEquals(
            __DIR__ . '/content-cache/hello/world.file',
            $method->invoke(new stdClass())
        );

        ContentCache::init('/testpath///');
        self::assertEquals(
            '/testpath/content-cache/hello/world.file',
            $method->invoke(new stdClass())
        );
    }

    public function testShouldCache(): void
    {
        $method = $this->getMethod('shouldCache');

        $_SERVER = ['QUERY_STRING' => ''];
        self::assertTrue($method->invoke(new stdClass()));

        $_SERVER = ['QUERY_STRING' => 'hello=world&answer=42'];
        self::assertFalse($method->invoke(new stdClass()));

        $_SERVER = ['QUERY_STRING' => ''];
        ContentCache::init(__DIR__, function () {
            return true;
        });
        self::assertTrue($method->invoke(new stdClass()));

        ContentCache::init(__DIR__, function () {
            return false;
        });
        self::assertFalse($method->invoke(new stdClass()));
    }

    public function testAddToCache(): void
    {
        ContentCache::init(__DIR__);

        $_SERVER = [
            'REQUEST_URI'  => '/hello/world',
            'QUERY_STRING' => ''
        ];

        self::assertTrue(ContentCache::addToCache('a happy penguin'));
        $fileName = __DIR__ . '/content-cache/hello/world.file';
        self::assertFileExists($fileName);
        self::assertEquals('a happy penguin', file_get_contents($fileName));

        self::assertTrue(ContentCache::addToCache('a happy penguin again'));
        self::assertEquals('a happy penguin again', file_get_contents($fileName));

        $_SERVER = [
            'REQUEST_URI'  => '/hello/world/again',
            'QUERY_STRING' => 'answer=42'
        ];
        self::assertFalse(ContentCache::addToCache('a happy penguin once more'));
        $fileName = __DIR__ . '/content-cache/hello/world/again.file';
        self::assertFileDoesNotExist($fileName);

        $_SERVER['QUERY_STRING'] = '';
        self::assertTrue(ContentCache::addToCache('a happy penguin once more'));
        self::assertFileExists($fileName);

        ContentCache::init('something');
        $this->expectException(InvalidConfigurationException::class);
        ContentCache::addToCache('a happy penguin once more');
    }

    public function testClearCache(): void
    {
        ContentCache::init(__DIR__);

        $paths = [
            '/',
            '/hippo',
            '/hello/world',
            '/hello/world-again',
            '/happy/penguin',
            '/happy/penguin-again',
        ];

        foreach ($paths as $path) {
            $_SERVER = ['REQUEST_URI' => $path, 'QUERY_STRING' => ''];
            self::assertTrue(ContentCache::addToCache('a happy penguin'));
        }

        ContentCache::clearCache('hello/world.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world.file');
        self::assertFileExists(__DIR__ . '/content-cache/hello/world-again.file');
        self::assertFileExists(__DIR__ . '/content-cache/happy/penguin.file');
        self::assertFileExists(__DIR__ . '/content-cache/happy/penguin-again.file');
        self::assertFileExists(__DIR__ . '/content-cache/hippo.file');
        self::assertFileExists(__DIR__ . '/content-cache/__index.file');

        ContentCache::clearCache('hello');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world-again.file');
        self::assertFileExists(__DIR__ . '/content-cache/happy/penguin.file');
        self::assertFileExists(__DIR__ . '/content-cache/happy/penguin-again.file');
        self::assertFileExists(__DIR__ . '/content-cache/hippo.file');
        self::assertFileExists(__DIR__ . '/content-cache/__index.file');

        ContentCache::clearCache('/happy/');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world-again.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/happy/penguin.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/happy/penguin-again.file');
        self::assertFileExists(__DIR__ . '/content-cache/hippo.file');
        self::assertFileExists(__DIR__ . '/content-cache/__index.file');

        ContentCache::clearCache();
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hello/world-again.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/happy/penguin.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/happy/penguin-again.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/hippo.file');
        self::assertFileDoesNotExist(__DIR__ . '/content-cache/__index.file');
    }

    private function getMethod(string $methodName): ReflectionMethod
    {
        ContentCache::init(__DIR__);

        $reflectedClass = new ReflectionClass(ContentCache::class);
        $method = $reflectedClass->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}